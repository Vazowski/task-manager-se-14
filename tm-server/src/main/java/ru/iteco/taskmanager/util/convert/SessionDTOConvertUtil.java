package ru.iteco.taskmanager.util.convert;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;

public class SessionDTOConvertUtil {

    @Nullable
    public static SessionDTO sessionToDTO(@Nullable final Session session) {
	if (session == null)
	    return null;

	@NotNull
	final SessionDTO sessionDTO = new SessionDTO();
	sessionDTO.setId(session.getId());
	sessionDTO.setTimestamp(session.getTimestamp());
	sessionDTO.setUserId(session.getUser().getId());
	sessionDTO.setSignature(session.getSignature());
	return sessionDTO;
    }

    @Nullable
    public static Session DTOToSession(@Nullable final SessionDTO sessionDTO) {
	if (sessionDTO == null)
	    return null;

	@NotNull
	final Session session = new Session();
	session.setId(session.getId());
	session.setTimestamp(session.getTimestamp());
	@Nullable
	User user = new User();
	user.setId(sessionDTO.getUserId());
	session.setUser(user);
	session.setSignature(session.getSignature());
	return session;
    }
}
