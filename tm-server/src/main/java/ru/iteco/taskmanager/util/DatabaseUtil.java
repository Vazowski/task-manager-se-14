package ru.iteco.taskmanager.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;

import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;

public final class DatabaseUtil {

    public static EntityManagerFactory factory() throws Exception {
	final InputStream inputStream = DatabaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
	final Properties property = new Properties();
	property.load(inputStream);

	final Map<String, String> settings = new HashMap<>();
	settings.put(Environment.DRIVER, property.getProperty("db.driver"));
	settings.put(Environment.URL, property.getProperty("db.host"));
	settings.put(Environment.USER, property.getProperty("db.login"));
	settings.put(Environment.PASS, property.getProperty("db.password"));
	settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
	settings.put(Environment.HBM2DDL_AUTO, "update");
	settings.put(Environment.SHOW_SQL, "true");

	final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
	registryBuilder.applySettings(settings);
	final StandardServiceRegistry registry = registryBuilder.build();
	final MetadataSources sources = new MetadataSources(registry);

	sources.addAnnotatedClass(Task.class);
	sources.addAnnotatedClass(Project.class);
	sources.addAnnotatedClass(User.class);
	sources.addAnnotatedClass(Session.class);
	final Metadata metadata = sources.getMetadataBuilder().build();
	return metadata.getSessionFactoryBuilder().build();
    }
}
