package ru.iteco.taskmanager.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DateUtil {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static String getDate(@Nullable final String date) {
	try {
	    return formatter.parse(date).toString();
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return null;
    }
}
