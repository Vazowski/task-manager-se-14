package ru.iteco.taskmanager.repository;

import javax.persistence.EntityManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Session;

public class SessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@Nullable final EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void merge(@NotNull final Session session) {
	entityManager.merge(session);
    }

    public void persist(@NotNull final Session session) {
	entityManager.persist(session);
    }

    @NotNull
    public Session findById(@NotNull final String id) {
	return entityManager.find(Session.class, id);
    }

    public void remove(@NotNull final String id) {
	entityManager.remove(entityManager.find(Session.class, id));
    }
}
