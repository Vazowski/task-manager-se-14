package ru.iteco.taskmanager.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @Nullable
    private TaskRepository taskRepository;

    public void merge(@NotNull final Task task) {
	if (task == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskRepository.merge(task);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void persist(@NotNull final Task task) {
	if (task == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskRepository.persist(task);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    @Nullable
    public Task findById(@NotNull final String id) {
	@Nullable
	Task task = null;
	if (id == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    task = taskRepository.findById(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return task;
    }

    @Nullable
    public Task findByProjectId(@NotNull final String projectId) {
	@Nullable
	Task task = null;
	if (projectId == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    task = taskRepository.findByProjectId(projectId);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return task;
    }

    @Nullable
    public Task findByName(@NotNull final String name) {
	@Nullable
	Task task = null;
	if (name == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    task = taskRepository.findByName(name);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return task;
    }

    @Nullable
    public List<Task> findAll() {
	@Nullable
	List<Task> taskList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskList = taskRepository.findAll();
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return taskList;
    }

    @Nullable
    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
	@Nullable
	List<Task> taskList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskList = taskRepository.findAllByOwnerId(ownerId);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return taskList;
    }

    @Nullable
    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
	@Nullable
	List<Task> taskList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskList = taskRepository.findAllByPartOfName(partOfName);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return taskList;
    }

    @Nullable
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@Nullable
	List<Task> taskList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskList = taskRepository.findAllByPartOfDescription(partOfDescription);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return taskList;
    }

    public void remove(@NotNull final String id) {
	if (id == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskRepository.remove(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void removeAll() {
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    taskRepository = new TaskRepository(entityManager);
	    entityManager.getTransaction().begin();
	    taskRepository.removeAll();
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }
}
