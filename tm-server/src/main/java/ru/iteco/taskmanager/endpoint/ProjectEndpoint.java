package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.dto.ProjectDTO;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void projectMerge(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "project") @Nullable final ProjectDTO projectDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getProjectService().merge(ProjectDTOConvertUtil.DTOToProject(projectDTO));
    }

    @WebMethod
    public void projectPersist(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "project") @Nullable final ProjectDTO projectDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getProjectService().persist(ProjectDTOConvertUtil.DTOToProject(projectDTO));
    }

    @WebMethod
    public @Nullable ProjectDTO findProjectById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectToDTO(serviceLocator.getProjectService().findById(id));
    }

    @WebMethod
    public @Nullable ProjectDTO findProjectByName(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "projectName") @Nullable String projectName) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectToDTO(serviceLocator.getProjectService().findByName(projectName));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProject(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(serviceLocator.getProjectService().findAll());
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByOwnerId(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "ownerId") @Nullable final String ownerId) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(serviceLocator.getProjectService().findAllByOwnerId(ownerId));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByPartOfName(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfName") @Nullable final String partOfName) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(serviceLocator.getProjectService().findAllByPartOfName(partOfName));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByPartOfDescription(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfDescription") @Nullable final String partOfDescription) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil
		.projectsToDTO(serviceLocator.getProjectService().findAllByPartOfDescription(partOfDescription));
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getProjectService().remove(id);
    }

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getProjectService().removeAll();
    }
}
