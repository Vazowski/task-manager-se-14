package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.TaskDTO;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void taskMerge(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "task") @Nullable final TaskDTO taskDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getTaskService().merge(TaskDTOConvertUtil.DTOToTask(taskDTO));
    }

    @WebMethod
    public void taskPersist(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "task") @Nullable final TaskDTO taskDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getTaskService().persist(TaskDTOConvertUtil.DTOToTask(taskDTO));
    }

    @WebMethod
    public @Nullable TaskDTO findTaskById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.taskToDTO(serviceLocator.getTaskService().findById(id));
    }

    @WebMethod
    public @Nullable TaskDTO findTaskByProjectId(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "projectId") @Nullable String projectId) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.taskToDTO(serviceLocator.getTaskService().findByProjectId(projectId));
    }

    @WebMethod
    public @Nullable TaskDTO findTaskByName(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "taskName") @Nullable String taskName) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.taskToDTO(serviceLocator.getTaskService().findByName(taskName));
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.tasksToDTO(serviceLocator.getTaskService().findAll());
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllTaskByOwnerId(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "ownerId") @Nullable String ownerId) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.tasksToDTO(serviceLocator.getTaskService().findAllByOwnerId(ownerId));
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllTaskByPartOfName(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfName") @Nullable String partOfName) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil.tasksToDTO(serviceLocator.getTaskService().findAllByPartOfName(partOfName));
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllTaskByPartOfDescription(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfDescription") @Nullable String partOfDescription) {
	SignatureUtil.validate(sessionDTO);
	return TaskDTOConvertUtil
		.tasksToDTO(serviceLocator.getTaskService().findAllByPartOfDescription(partOfDescription));
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getTaskService().remove(id);
    }

    @WebMethod
    public void removeAllTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getTaskService().removeAll();
    }
}