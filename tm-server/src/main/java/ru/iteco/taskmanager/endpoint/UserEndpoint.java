package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void merge(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "user") @NotNull UserDTO userDTO) {
	SignatureUtil.validate(sessionDTO);
	serviceLocator.getUserService().merge(UserDTOConvertUtil.DTOToUser(userDTO));
    }

    @WebMethod
    public void persist(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "user") @NotNull UserDTO userDTO) {
	SignatureUtil.validate(sessionDTO);
	serviceLocator.getUserService().persist(UserDTOConvertUtil.DTOToUser(userDTO));
    }

    @WebMethod
    @Nullable
    public UserDTO findUserById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @NotNull String id) {
	SignatureUtil.validate(sessionDTO);
	return UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(id));
    }

    @WebMethod
    @Nullable
    public List<UserDTO> findAllUser(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	SignatureUtil.validate(sessionDTO);
	return UserDTOConvertUtil.usersToDTO(serviceLocator.getUserService().findAll());
    }

    @WebMethod
    @Nullable
    public String getCurrent(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	SignatureUtil.validate(sessionDTO);
	return serviceLocator.getUserService().getCurrentUser().toString();
    }

    @WebMethod
    public void setCurrent(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "login") @NotNull String login) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	serviceLocator.getUserService().setCurrentUser(new StringBuilder(login));
    }
}
