package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.dto.DomainDTO;
import ru.iteco.taskmanager.dto.ProjectDTO;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.TaskDTO;
import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

    IServiceLocator serviceLocator;

    @WebMethod
    public DomainDTO saveData(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return serviceLocator.getDomainService().saveData(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO loadData(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return serviceLocator.getDomainService().loadData();
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbSaveXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return serviceLocator.getDomainService().jaxbSaveXml(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbLoadXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return serviceLocator.getDomainService().jaxbLoadXml();
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbSaveJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return serviceLocator.getDomainService().jaxbSaveJson(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbLoadJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return serviceLocator.getDomainService().jaxbLoadJson();
    }

    @WebMethod
    public DomainDTO jacksonSaveXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return serviceLocator.getDomainService().jacksonSaveXml(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jacksonLoadXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return serviceLocator.getDomainService().jacksonLoadXml();
    }

    @WebMethod
    public DomainDTO jacksonSaveJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return serviceLocator.getDomainService().jacksonSaveJson(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jacksonLoadJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(serviceLocator.getUserService().findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return serviceLocator.getDomainService().jacksonLoadJson();
    }

    private DomainDTO makeDomain() {
	@NotNull
	DomainDTO domain = new DomainDTO();
	@Nullable
	final List<UserDTO> userList = UserDTOConvertUtil.usersToDTO(serviceLocator.getUserService().findAll());
	if (userList != null)
	    domain.setUserList(userList);
	@Nullable
	final List<ProjectDTO> projectList = ProjectDTOConvertUtil
		.projectsToDTO(serviceLocator.getProjectService().findAll());
	if (projectList != null)
	    domain.setProjectList(projectList);
	@Nullable
	final List<TaskDTO> taskList = TaskDTOConvertUtil.tasksToDTO(serviceLocator.getTaskService().findAll());
	if (taskList != null)
	    domain.setTaskList(taskList);

	return domain;
    }
}
