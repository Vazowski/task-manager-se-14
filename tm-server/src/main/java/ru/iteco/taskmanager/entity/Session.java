package ru.iteco.taskmanager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity {

    @Column
    private String timestamp;

    @Column
    private String signature;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;
}