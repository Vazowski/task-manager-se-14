package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.dto.ProjectDTO;
import ru.iteco.taskmanager.dto.SessionDTO;

@WebService
public interface IProjectEndpoint {

    void projectMerge(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "project") final ProjectDTO projectDTO);

    @WebMethod
    public void projectPersist(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "project") final ProjectDTO projectDTO);

    @WebMethod
    public ProjectDTO findProjectById(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "id") String id);

    @WebMethod
    public ProjectDTO findProjectByName(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "projectName") String projectName);

    @WebMethod
    public List<ProjectDTO> findAllProject(@WebParam(name = "session") final SessionDTO sessionDTO);

    @WebMethod
    public List<ProjectDTO> findAllProjectByOwnerId(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "ownerId") final String ownerId);

    @WebMethod
    public List<ProjectDTO> findAllProjectByPartOfName(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "partOfName") final String partOfName);

    @WebMethod
    public List<ProjectDTO> findAllProjectByPartOfDescription(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "partOfDescription") final String partOfDescription);

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "id") String id);

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") final SessionDTO sessionDTO);
}
