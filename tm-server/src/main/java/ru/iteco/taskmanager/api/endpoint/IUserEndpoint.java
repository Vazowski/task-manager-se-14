package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.UserDTO;

@WebService
public interface IUserEndpoint {

    @WebMethod
    void merge(@WebParam(name = "session") SessionDTO sessionDTO, @WebParam(name = "user") UserDTO userDTO);

    @WebMethod
    void persist(@WebParam(name = "session") SessionDTO sessionDTO, @WebParam(name = "user") UserDTO userDTO);

    @WebMethod
    UserDTO findUserById(@WebParam(name = "session") SessionDTO sessionDTO, @WebParam(name = "id") String id);

    @WebMethod
    List<UserDTO> findAllUser(@WebParam(name = "session") final SessionDTO sessionDTO);

    @WebMethod
    String getCurrent(@WebParam(name = "session") SessionDTO sessionDTO);

    @WebMethod
    void setCurrent(@WebParam(name = "session") SessionDTO sessionDTO, @WebParam(name = "login") String login);
}
