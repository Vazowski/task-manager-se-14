package ru.iteco.taskmanager.command.system;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
	return "help";
    }

    @Override
    public String description() {
	return "  -  show all commands";
    }

    @Override
    public void execute() throws Exception {
	System.out.println();
	for (final String key : serviceLocator.getTerminalService().getCommands().keySet()) {
	    if (!key.equals("login") && !key.equals("login")) {
		System.out.println(key + serviceLocator.getTerminalService().getCommands().get(key).description());
	    }
	}
    }
}
